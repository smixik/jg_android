package com.example.den.lesson9_firebase_test.Interfaces;

import android.app.Activity;
import android.widget.ImageView;

import com.example.den.lesson9_firebase_test.R;

public interface PhotoItemsPresenter {
    void setupWithPhotoItems(PhotoItem[] photoItems, Activity activity, PhotoItemsPresenterCallbacks callback);
    void updateWithItems(PhotoItem[] photoItems);

    default void updateFavoriteButton(ImageView imageFavorite, PhotoItem photoItem) {
        boolean isFavorited = photoItem.isSavedToDatabase();
        if(isFavorited) {
            imageFavorite.setImageResource(R.drawable.favorite_on);
        } else {
            imageFavorite.setImageResource(R.drawable.favorite_off);
        }
    }
}
