package com.example.den.lesson9_firebase_test.Presenters.RecyclerView;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.den.lesson9_firebase_test.Interfaces.PhotoItem;
import com.example.den.lesson9_firebase_test.Interfaces.PhotoItemsPresenter;
import com.example.den.lesson9_firebase_test.Interfaces.PhotoItemsPresenterCallbacks;
import com.example.den.lesson9_firebase_test.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolderRecyclerView> {

    PhotoItem[] photoItems;
    PhotoItemsPresenter presenter;
    PhotoItemsPresenterCallbacks callback;
    private static ClickListener clickListener;

    public Adapter(PhotoItem[] photoItems, PhotoItemsPresenter presenter, PhotoItemsPresenterCallbacks callback) {
        this.presenter = presenter;
        this.photoItems = photoItems;
        this.callback = callback;
        this.setOnItemClickListener((int position, View v) -> callback.onItemSelected(photoItems[position]));
    }


    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolderRecyclerView onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item_img, parent, false);
        return new ViewHolderRecyclerView(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderRecyclerView holder, int position) {

        PhotoItem photoItem = this.photoItems[position];

        holder.textViewAuthor.setText(photoItem.getAuthorName());
        Picasso.get().load(photoItem.getImgUrl()).placeholder(R.drawable.placeholder).into(holder.imageViewPhoto);
        presenter.updateFavoriteButton(holder.imageFavorite, photoItem);
        holder.imageFavorite.setOnClickListener((View view) -> {
            callback.onItemToggleFavorite(photoItem);
            presenter.updateFavoriteButton(holder.imageFavorite, photoItem);
        });

    }

    @Override
    public int getItemCount() {
        return this.photoItems.length;
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        Adapter.clickListener = clickListener;
    }

    public static class ViewHolderRecyclerView extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.imageView)
        public ImageView imageViewPhoto;
        @BindView(R.id.textViewAuthor)
        public TextView textViewAuthor;
        @BindView(R.id.imageFavorite)
        public ImageView imageFavorite;

        ViewHolderRecyclerView(View view) {
            super(view);
            view.setOnClickListener(this);
            ButterKnife.bind(this, view);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }
}
