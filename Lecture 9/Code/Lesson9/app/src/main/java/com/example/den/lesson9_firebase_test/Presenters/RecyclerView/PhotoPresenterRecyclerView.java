package com.example.den.lesson9_firebase_test.Presenters.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;


import com.example.den.lesson9_firebase_test.Interfaces.PhotoItem;
import com.example.den.lesson9_firebase_test.Interfaces.PhotoItemsPresenter;
import com.example.den.lesson9_firebase_test.Interfaces.PhotoItemsPresenterCallbacks;
import com.example.den.lesson9_firebase_test.R;

public class PhotoPresenterRecyclerView implements PhotoItemsPresenter {

    private RecyclerView mRecyclerView;
    private Adapter mAdapter;

    @Override
    public void setupWithPhotoItems(PhotoItem[] photoItems, Activity activity, PhotoItemsPresenterCallbacks callback) {

        this.mRecyclerView = new RecyclerView(activity);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        this.mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(activity,2);
        this.mRecyclerView.setLayoutManager(mLayoutManager);

        this.mAdapter = new Adapter(photoItems, this, callback);
        this.mRecyclerView.setAdapter(mAdapter);

        this.mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int pastVisibleItems = ((GridLayoutManager)mLayoutManager).findFirstVisibleItemPosition();
                if (pastVisibleItems + visibleItemCount + 10 >= totalItemCount) {
                    callback.onLastItemReach(totalItemCount);
                }
            }
        });

        activity.setContentView(mRecyclerView);
    }

    @Override
    public void updateWithItems(PhotoItem[] photoItems) {
        this.mAdapter.photoItems = photoItems;
        this.mAdapter.notifyDataSetChanged();
    }
}
