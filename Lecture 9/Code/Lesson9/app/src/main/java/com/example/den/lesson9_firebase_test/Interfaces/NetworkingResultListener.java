package com.example.den.lesson9_firebase_test.Interfaces;

public interface NetworkingResultListener {
    void callback(PhotoItem[] photoItems);
}
