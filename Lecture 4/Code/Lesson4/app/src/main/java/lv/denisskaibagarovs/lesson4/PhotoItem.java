package lv.denisskaibagarovs.lesson4;

import java.util.Map;

public class PhotoItem {
    Map<String,String> urls;
    User user;

    public String getImgUrl() {
        return this.urls.get("regular");
    }

    public String getAuthorName() {
        return this.user.name;
    }

    public String getLocation() {
        return this.user.location;
    }

    public class User {
        String name;
        String location;
    }
}
