package com.example.den.lesson7homework.Interfaces;

public interface NetworkingResultListener {
    void callback(PhotoItem[] photoItems);
}
