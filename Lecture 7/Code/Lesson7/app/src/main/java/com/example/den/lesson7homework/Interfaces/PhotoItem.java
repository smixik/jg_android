package com.example.den.lesson7homework.Interfaces;

import android.content.Context;
import android.content.Intent;

import com.example.den.lesson7homework.ShareActivity;
import com.example.den.lesson7homework.ShareActivityWithFragments;

import java.io.Serializable;

public interface PhotoItem extends Serializable {
    String getID();
    String getImgUrl();
    String getAuthorName();

    // ORM
    void saveToDatabase();
    void deleteFromDatabase();
    boolean isSavedToDatabase();

    default void toShareActivity(Context context){
        Intent shareIntent = new Intent(context, ShareActivityWithFragments.class);
        shareIntent.putExtra(ShareActivity.PHOTO_ITEM_KEY, this);
        context.startActivity(shareIntent);
    }

    default void testFavoriteORM(){
        if(this.isSavedToDatabase()) {
            this.deleteFromDatabase();
        } else {
            this.saveToDatabase();
        }
    }
}
