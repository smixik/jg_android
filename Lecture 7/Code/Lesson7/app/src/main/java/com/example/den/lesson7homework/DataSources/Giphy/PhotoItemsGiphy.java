package com.example.den.lesson7homework.DataSources.Giphy;

import com.example.den.lesson7homework.DataSources.Unsplash.PhotoItemUnsplash;
import com.example.den.lesson7homework.Interfaces.PhotoItem;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.io.Serializable;
import java.util.List;

@Table
public class PhotoItemsGiphy implements PhotoItem  {

    @SerializedName("db_id")
    private transient Long id;

    @SerializedName("id")
    private String imgID;

    ImagesContainer images;
    String username;

    private String imagesForORM;
    private boolean deleted;

    public PhotoItemsGiphy() { }

    @Override
    public String getID() {return this.imgID;}

    @Override
    public String getImgUrl() {
        if(images == null) {
            this.images = new Gson().fromJson(this.imagesForORM, ImagesContainer.class);
        }

        return images.getMediumSize();
    }

    @Override
    public String getAuthorName() {
        return username;
    }

    @Override
    public void saveToDatabase() {
        if(!isSavedToDatabase()){
            this.imagesForORM = images.toString();
            this.deleted = false;
            SugarRecord.save(this);
        }
    }

    @Override
    public void deleteFromDatabase() {
        this.deleted = true;
        //SugarRecord.deleteAll(PhotoItemsGiphy.class,"img_ID = ?", this.imgID);
        SugarRecord.save(this);
    }

    @Override
    public boolean isSavedToDatabase() {
        return Select.from(PhotoItemsGiphy.class)
                .where(Condition.prop("img_ID").eq(this.imgID),
                        Condition.prop("deleted").eq(0))
                .count() > 0;
    }

    public class ImagesContainer implements Serializable {

        public String toString() {
            return new Gson().toJson(this);
        }

        DownsizedMedium downsized_medium;

        public String getMediumSize() {
            return downsized_medium.url;
        }
    }

    public class DownsizedMedium implements Serializable {
        String url;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
