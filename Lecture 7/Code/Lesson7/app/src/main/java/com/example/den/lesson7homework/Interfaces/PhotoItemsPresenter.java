package com.example.den.lesson7homework.Interfaces;

import android.app.Activity;

public interface PhotoItemsPresenter {
    void setupWithPhotoItems(PhotoItem[] photoItems, Activity activity, PhotoItemsPresenterCallbacks callback);
}
