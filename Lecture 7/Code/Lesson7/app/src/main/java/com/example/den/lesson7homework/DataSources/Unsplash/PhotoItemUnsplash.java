package com.example.den.lesson7homework.DataSources.Unsplash;

import com.example.den.lesson7homework.DataSources.Giphy.PhotoItemsGiphy;
import com.example.den.lesson7homework.Interfaces.PhotoItem;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Table;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.io.Serializable;

@Table
public class PhotoItemUnsplash implements PhotoItem {

    @SerializedName("db_id")
    private transient Long id = null;

    @SerializedName("id")
    private String imgID;

    URLs urls;
    User user;

    private String URLsFromORM;
    private String UserFromORM;
    private boolean deleted;

    public PhotoItemUnsplash() {}


    public String getID () {return this.imgID;}

    public String getImgUrl() {
        if (urls == null) {
            this.urls = new Gson().fromJson(this.URLsFromORM, URLs.class);
        }

        return this.urls.regular;
    }

    public String getAuthorName() {
        if (user == null) {
            this.user = new Gson().fromJson(this.UserFromORM, User.class);
        }

        return this.user.name;
    }

    @Override
    public void saveToDatabase() {
        if(!isSavedToDatabase()) {
            this.URLsFromORM = urls.toString();
            this.UserFromORM = user.toString();
            this.deleted = false;

            SugarRecord.save(this);
        }
    }

    @Override
    public void deleteFromDatabase() {
        this.deleted = true;
        //SugarRecord.deleteAll(PhotoItemUnsplash.class,"img_ID = ?", this.imgID);
        SugarRecord.save(this);
    }

    @Override
    public boolean isSavedToDatabase() {
        return Select.from(PhotoItemUnsplash.class)
                .where(Condition.prop("img_ID").eq(this.imgID),
                        Condition.prop("deleted").eq(0))
                .count() > 0;
    }

    public class User implements Serializable {
        String name;

        public String toString() {
            return new Gson().toJson(this);
        }
    }

    public class URLs implements Serializable {
        String regular;

        public String toString() {
            return new Gson().toJson(this);
        }

    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
