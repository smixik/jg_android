package com.example.den.lesson7homework;

import android.app.Activity;
import android.os.Bundle;

import com.example.den.lesson7homework.DataSources.Giphy.PhotoItemsGiphy;
import com.example.den.lesson7homework.DataSources.Unsplash.PhotoItemUnsplash;
import com.example.den.lesson7homework.Interfaces.PhotoItem;
import com.example.den.lesson7homework.Interfaces.PhotoItemsPresenterCallbacks;
import com.example.den.lesson7homework.Presenters.PhotoItemPresenterGridView;
import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

import static com.orm.util.Collection.list;

public class FavoriteActivity extends Activity implements PhotoItemsPresenterCallbacks {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new PhotoItemPresenterGridView().setupWithPhotoItems( getAllSavedPhotoItems(),this, this);
    }

    @Override
    public void onItemSelected(PhotoItem item) {
        item.toShareActivity(this);
    }

    @Override
    public void onItemToggleFavorite(PhotoItem item) {
        item.testFavoriteORM();
    }

    private PhotoItem[] getAllSavedPhotoItems() {
        List<PhotoItem> allFavoritedItems = new ArrayList<>(Select.from(PhotoItemsGiphy.class)
                .where(Condition.prop("deleted").eq(0))
                .list());
        allFavoritedItems.addAll(Select.from(PhotoItemUnsplash.class)
                .where(Condition.prop("deleted").eq(0))
                .list());
        return allFavoritedItems.toArray(new PhotoItem[allFavoritedItems.size()]);
    }
}

