package lv.maksimssenko.lesson2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void buttonPressAct(View view) {
        EditText email = findViewById(R.id.email);
        EditText pass = findViewById(R.id.pass);
        TextView errorText = findViewById(R.id.loginErrorText);
        if("admin".equals(email.getText().toString()) && "admin".equals(pass.getText().toString())){
            Intent form = new Intent(getBaseContext(),   FormActivity.class);
            startActivity(form);
            errorText.setVisibility(View.GONE);
        } else {
            errorText.setVisibility(View.VISIBLE);
        }
    }
}
