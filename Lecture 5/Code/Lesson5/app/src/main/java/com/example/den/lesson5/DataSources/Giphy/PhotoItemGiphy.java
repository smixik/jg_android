package com.example.den.lesson5.DataSources.Giphy;

import com.example.den.lesson5.Interfaces.PhotoItem;

public class PhotoItemGiphy implements PhotoItem {

    Images images;
    User user;

    public String getImgUrl() {
        return this.images.downsized_medium.url;
    }

    public String getAuthorName() {
        if(user == null) return "";
        return this.user.display_name;
    }

    private class Images {
        ImageData downsized_medium;
    }

    private class ImageData {
        String url;
    }

    public class User {
        String display_name;
    }
}
