package lv.denisskaibagarovs.lesson1;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Button button = findViewById(R.id.button);
        button.setBackgroundColor(Color.GREEN);

        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        RelativeLayout mainLayout = findViewById(R.id.mainLayout);
        mainLayout.setBackgroundColor(color);
    }

    @Override
    protected void onPause() {
        super.onPause();
        RelativeLayout mainLayout = findViewById(R.id.mainLayout);
        mainLayout.setBackgroundColor(Color.BLUE);
    }

    public void onButtonPress(View view) {
        Button button = findViewById(R.id.button);
        button.setBackgroundColor(Color.RED);
    }
}
