package lv.maksimssenko.lesson3;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Resources res = getResources();

        final List<Car> cars = new ArrayList<>();
        final String[] carsNameArray = res.getStringArray(R.array.car_types);
        for (int i = 0; i < carsNameArray.length; i++) {
            String carName = carsNameArray[i];
            int resourceId = this.getResources().getIdentifier(carName.toLowerCase(), "drawable", getPackageName());
            Drawable carImage = getResources().getDrawable(resourceId);
            cars.add(new Car(carName, carImage));
        }

        ArrayAdapter<Car> carAdapterAdvanced =
                new ArrayAdapter<Car>(this, 0, cars) {
                    @Override
                    public View getView(int position,
                                        View convertView,
                                        ViewGroup parent) {

                        if (convertView == null) {
                            convertView = getLayoutInflater()
                                    .inflate(R.layout.custom_item_img, null, false);
                        }

                        if (convertView.getTag() == null) {
                            ViewHolder viewHolder = new ViewHolder();
                            viewHolder.image = convertView.findViewById(R.id.imageView);
                            viewHolder.name = convertView.findViewById(R.id.name);
                            convertView.setTag(viewHolder);
                        }

                        ImageView image = ((ViewHolder) convertView.getTag()).image;
                        TextView name = ((ViewHolder) convertView.getTag()).name;

                        name.setText(cars.get(position).name);
                        image.setImageDrawable(cars.get(position).image);

                        return convertView;
                    }
                };

        GridView carGrid = new GridView(this);
        setContentView(carGrid);
        carGrid.setNumColumns(2);
        carGrid.setColumnWidth(60);
        carGrid.setVerticalSpacing(20);
        carGrid.setHorizontalSpacing(20);
        carGrid.setAdapter(carAdapterAdvanced);

        carGrid.setOnItemClickListener((parent, view, position, id) -> {
            // Generate a message based on the position
            String message = "You clicked on " + cars.get(position).name;
            Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
            toast.show();
        });
    }
}
