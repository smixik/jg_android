package lv.maksimssenko.lesson3;

import android.graphics.drawable.Drawable;

public class Car {

    String name;
    Drawable image;

    public Car(String name, Drawable image) {
        this.name = name;
        this.image = image;
    }
}
