package com.example.den.lesson6.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.den.lesson6.Interfaces.PhotoItem;
import com.example.den.lesson6.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class InfoFragment extends Fragment {

    public interface InfoFragmentListener {
        void onClosePress();
    }

    public PhotoItem photoItem;
    private InfoFragmentListener listener;


    @BindView(R.id.imageAuthor)
    TextView imageAuthor;
    @BindView(R.id.closeInfoBtn)
    Button closeInfoBtn;

    public InfoFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InfoFragmentListener) {
            listener = (InfoFragmentListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement ShareFragmentListener");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        ButterKnife.bind(this, view);
        imageAuthor.setText(photoItem.getAuthorName());

        closeInfoBtn.setOnClickListener(button -> listener.onClosePress());

        return view;
    }

}
